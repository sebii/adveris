<?php

namespace App\Command;

use App\Entity\Company;
use App\Entity\Project;
use App\Entity\Technology;
use App\Entity\Type;
use App\Entity\Worker;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ImportXmlCommand extends Command
{
    protected static $defaultName = 'app:import:xml';
    protected static $defaultDescription = 'Add a short description for your command';
    protected EntityManagerInterface $em;
    protected Serializer $serializer;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->serializer = new Serializer();
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('xmlpath', InputArgument::REQUIRED, 'path of xml file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $xmlPath = $input->getArgument('xmlpath');
        $fs = new Filesystem();

        try {
            if (!$fs->exists($xmlPath)) {
                throw new \Exception('XML import file does not exist.');
            }
            $xml = simplexml_load_file($xmlPath);
            $json = json_encode($xml);
            $data = json_decode($json, TRUE);
            foreach ($data['project'] as $projectData) {
                $project = $this->em->getRepository(Project::class)->getOneByIdOrNewIt($projectData['id']);
                $type = $this->em->getRepository(Type::class)->getOneByNameOrNewIt($projectData['type']);
                $technology = $this->em->getRepository(Technology::class)->getOneByNameOrNewIt($projectData['technology']);
                $company = $this->em->getRepository(Company::class)->getOneByNameOrNewIt($projectData['company']);

                $project
                    ->setId($projectData['id'])
                    ->setStartDate(new \DateTime($projectData['start_date']))
                    ->setName($projectData['name'])
                    ->setPriceSold($projectData['price_sold'])
                    ->setType($type)
                    ->setEstimatedTimeToCompletion($projectData['times']['estimated_time_to_completion'] ?? null)
                    ->setSpentTime($projectData['times']['spent_time'] ?? null)
                    ->setTechnology($technology)
                    ->setCompany($company);

                foreach(explode(', ', $projectData['workers']) as $workerData) {
                    $worker = $this->em->getRepository(Worker::class)->getOneByNameOrNewIt($workerData);
                    $project->addWorker($worker);
                }

                if (!$this->em->contains($project)) {
                    $this->em->persist($project);
                }
                $this->em->flush();
            }
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }

        $io->success('Schema imported');

        return Command::SUCCESS;
    }
}
