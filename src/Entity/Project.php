<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private \DateTime $start_date;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $price_sold;

    /**
     * @var Type
     * @ORM\ManyToOne(targetEntity=Type::class)
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private Type $type;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $estimatedTimeToCompletion;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $spentTime;

    /**
     * @var Technology
     * @ORM\ManyToOne(targetEntity=Technology::class)
     * @ORM\JoinColumn(name="technology_id", referencedColumnName="id", nullable=false)
     */
    private Technology $technology;

    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity=Company::class)
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=false)
     */
    private Company $company;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity=Worker::class)
     * @ORM\JoinTable(name="project_x_worker",
     *     joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn("worker_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"name"="ASC"})
     */
    private Collection $workers;

    public function __construct()
    {
        $this->name = '';
        $this->workers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Project
     */
    public function setId(int $id): Project
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->start_date;
    }

    /**
     * @param \DateTime $start_date
     * @return Project
     */
    public function setStartDate(\DateTime $start_date): Project
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Project
     */
    public function setName(string $name): Project
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceSold(): int
    {
        return $this->price_sold;
    }

    /**
     * @param int $price_sold
     * @return Project
     */
    public function setPriceSold(int $price_sold): Project
    {
        $this->price_sold = $price_sold;
        return $this;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @param Type $type
     * @return Project
     */
    public function setType(Type $type): Project
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getEstimatedTimeToCompletion(): ?int
    {
        return $this->estimatedTimeToCompletion;
    }

    /**
     * @param int|null $estimatedTimeToCompletion
     * @return Project
     */
    public function setEstimatedTimeToCompletion(?int $estimatedTimeToCompletion): Project
    {
        $this->estimatedTimeToCompletion = $estimatedTimeToCompletion;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSpentTime(): ?int
    {
        return $this->spentTime;
    }

    /**
     * @param int|null $spentTime
     * @return Project
     */
    public function setSpentTime(?int $spentTime): Project
    {
        $this->spentTime = $spentTime;
        return $this;
    }

    /**
     * @return Technology
     */
    public function getTechnology(): Technology
    {
        return $this->technology;
    }

    /**
     * @param Technology $technology
     * @return Project
     */
    public function setTechnology(Technology $technology): Project
    {
        $this->technology = $technology;
        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return Project
     */
    public function setCompany(Company $company): Project
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getWorkers(): Collection
    {
        return $this->workers;
    }

    /**
     * @param Collection $workers
     * @return Project
     */
    public function setWorkers($workers): self
    {
        $this->workers = $workers;
        return $this;
    }
    
    public function addWorker(Worker $worker): Project
    {
        if (!$this->workers->contains($worker)) {
            $this->workers->add($worker);
        }
        return $this;
    }

    public function removeWorker(Worker $worker): Project
    {
        if ($this->workers->contains($worker)) {
            $this->workers->removeElement($worker);
        }
        return $this;
    }
}
