<?php

namespace App\Datatable;

use App\Entity\Project;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableTypeInterface;

class ProjectDatatableType implements DataTableTypeInterface
{
    public function configure(DataTable $dataTable, array $options)
    {
        $dataTable
            ->setName('project-dt')
            ->add('id', TextColumn::class, ['label' => 'project.id'])
            ->add('name', TextColumn::class, ['label' => 'project.name'])
            ->add('priceSold', TextColumn::class, ['label' => 'project.pricesold'])
            ->add('type', TextColumn::class, ['label' => 'project.type'])
            ->add('technology', TextColumn::class, ['label' => 'project.technology'])
            ->add('company', TextColumn::class, ['label' => 'project.company'])
            ->add('workers', TextColumn::class, [
                'label' => 'project.workers',
                'render' => function($value, $context) {
                    $workersNames = [];
                    foreach ($context->getWorkers() as $worker) {
                        $workersNames[] = $worker->getName();
                    }
                    return implode(', ', $workersNames);
                }
            ])
            ->add('_link', TextColumn::class, [
                'label' => 'link',
                'render' => function($value, $context) {
                    return '<a href="#">voir</a>';
                }
            ])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Project::class,
            ]);
    }
}