<?php

namespace App\Controller;

use App\Datatable\ProjectDatatableType;
use App\Entity\Project;
use App\Form\ProjectFormType;
use Doctrine\ORM\EntityManagerInterface;
use Omines\DataTablesBundle\DataTableFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/", name="app_project_index", methods={"get"})
     */
    public function index(EntityManagerInterface $em): Response
    {
        return $this->render('project/index.html.twig', [
            'projects' => $em->getRepository(Project::class)->findAll(),
        ]);
    }
//    public function index(Request $request, DataTableFactory $dataTableFactory): Response
//    {
//        $projectsDT = $dataTableFactory->createFromType(ProjectDatatableType::class)->handleRequest($request);
//        if ($projectsDT->isCallback()) {
//            return $projectsDT->getResponse();
//        }
//        return $this->render('project/index.html.twig', [
//            'projectsDT' => $projectsDT,
//        ];
//    }

    /**
     * @param Project $project
     * @return array
     * @Route("/project/{projectId}", name="app_project_view", methods={"get"}, requirements={"projectId"="\d+"})
     * @Entity("project", expr="repository.find(projectId)")
     * @Template("project/view.html.twig")
     */
    public function view(Project $project): array
    {
        return [
            'project' => $project,
        ];
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Project|null $project
     * @return Response
     * @Route("/project/add", name="app_project_add", methods={"get", "post"}, defaults={"mode"="add", "projectId"=0})
     * @Route("/project/{projectId}/edit", name="app_project_edit", defaults={"mode"="edit"}, requirements={"projectId"="\d+"})
     * @Entity("project", expr="repository.find(projectId)")
     */
    public function form(Request $request, EntityManagerInterface $em, ?Project $project, string $mode): Response
    {
        switch ($mode) {
            case 'add':
                $project = new Project();
                $actionPath = $this->generateUrl('app_project_add');
                break;
            case 'edit':
                $actionPath = $this->generateUrl('app_project_edit', ['projectId' => $project->getId()]);
                break;
            default:
                throw new \Exception('Mode not allowed');
        }
        $form = $this->createForm(ProjectFormType::class, $project, ['action' => $actionPath]);
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if (!$em->contains($project)) {
                $em->persist($project);
                $em->flush();
                $em->refresh($project);
            } else {
                $em->flush();
            }
            return $this->redirectToRoute('app_project_view', ['projectId' => $project->getId()]);
        }
        return $this->render('project/form.html.twig', [
            'mode' => $mode,
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }
}
