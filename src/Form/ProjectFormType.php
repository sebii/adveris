<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Project;
use App\Entity\Technology;
use App\Entity\Type;
use App\Entity\Worker;
use MyGHTY\AdminBundle\Entity\Params\Param;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'project.name'])
            ->add('startDate', DateType::class, ['label' => 'project.startdate'])
            ->add('priceSold', IntegerType::class, ['label' => 'project.pricesold'])
            ->add('type', EntityType::class, [
                'label' => 'project.type',
                'class' => Type::class,
                'choice_label' => 'name'
            ])
            ->add('estimatedTimeToCompletion', IntegerType::class, ['label' => 'project.estimatedtimetocompletion'])
            ->add('spentTime', IntegerType::class, ['label' => 'project.spenttime'])
            ->add('technology', EntityType::class, [
                'label' => 'project.technology',
                'class' => Technology::class,
                'choice_label' => 'name'
            ])
            ->add('company', EntityType::class, [
                'label' => 'project.company',
                'class' => Company::class,
                'choice_label' => 'name'
            ])
            ->add('workers', EntityType::class, [
                'label' => 'project.workers',
                'class' => Worker::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'submit'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Project::class
        ]);
    }
}